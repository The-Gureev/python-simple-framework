from api import API
from webob import Request, Response

app = API()

@app.route(path = '/home')
def home(request: Request, response: Response):
    response.text = "Index"

@app.route(path = '/about')
def about(request: Request, response: Response):
    response.text = "About"

@app.route('/hello/{name}')
def about(request: Request, response: Response, name: str):
    response.text = f"Hello, {name}"