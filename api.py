from typing import Iterable
from typing import Callable
from typing import Union
from socks import method
from webob import Request, Response
from parse import parse

class API:
    def __init__(self):
        self.routes = {}
    
    def route(self, path: str):
        debug_point = 1

        def wrapper(handler: callable)-> callable:
            self.routes[path] = handler
            return handler
        return wrapper

    def __call__(self, environ: dict, start_response: method) -> Iterable:
        request = Request(environ)
        response = self.handle_request(request)
        return response(environ, start_response)
    
    def find_handler(self, request_path: str)-> Union[tuple[Callable, dict[str, str]], tuple[None, None]]:
        for path, handler in self.routes.items():
            parse_result = parse(path, request_path)
            if parse_result is not None:
                return handler, parse_result.named
        return None, None

    def handle_request(self, request: Request) -> Response:
        response = Response()
   
        handler, kwargs = self.find_handler(request_path=request.path)
            
        if handler is not None:
            handler(request, response, **kwargs)
        else:
            self.default_response(response)
        return response


    